<?php

namespace Uncgits\ZoomApiLaravel;

use Carbon\Carbon;
use Uncgits\ZoomApi\ZoomApiEndpoint;
use Uncgits\ZoomApi\ZoomApi as BaseApi;
use Uncgits\ZoomApi\Clients\ZoomApiClientInterface;
use Uncgits\ZoomApiLaravel\Events\ApiRequestStarted;
use Uncgits\ZoomApiLaravel\Events\ApiRequestFinished;

class ZoomApi extends BaseApi
{
    protected $cacheBypassed = false;

    public function __construct()
    {
        // set default adapter if specified
        $defaultAdapter = config('zoom-api.defaults.adapter');
        if (!empty($defaultAdapter)) {
            $this->setAdapter($defaultAdapter);

            // if we have default adapter, set default config if specified
            $defaultConfigKey = config('zoom-api.defaults.config');
            if (!empty($defaultConfigKey)) {
                $this->setConfig(config('zoom-api.configs.' . $defaultConfigKey . '.class'));
            }
        }
    }

    public function execute(ZoomApiClientInterface $client, ZoomApiEndpoint $endpoint, $method, $arguments)
    {
        event(new ApiRequestStarted($client, $endpoint, $method, $arguments));

        if (!$this->isCacheable($client, $endpoint, $method, $arguments)) {
            $this->refreshToken();

            $result = new ZoomApiResult($this->adapter->usingConfig($this->config)->transaction($endpoint));
            $result->setSource('api');

            event(new ApiRequestFinished($client, $endpoint, $method, $arguments, $result));
            return $result;
        }


        // figure out cache key
        $cacheKey = 'zoom-api-' . hash('sha3-512', serialize($client) . serialize($endpoint) . $method . json_encode($arguments));

        // check for the cached result first
        if (!is_null($cached = \Cache::get($cacheKey))) {
            event(new ApiRequestFinished($client, $endpoint, $method, $arguments, unserialize($cached)));
            return unserialize($cached)
                ->setSource('cache')
                ->setCacheKey($cacheKey);
        }

        // wasn't cached, so execute the call
        $this->refreshToken();
        $result = new ZoomApiResult($this->adapter->usingConfig($this->config)->transaction($endpoint));

        if ($result->getStatus() === 'success') {
            // it should be cached if it was successful
            \Cache::put($cacheKey, serialize($result), now()->addMinutes(config('zoom-api.cache_minutes', 10)));
        }

        event(new ApiRequestFinished($client, $endpoint, $method, $arguments, $result));
        return $result->setCacheKey($cacheKey);
    }

    public function withoutCache()
    {
        $this->cacheBypassed = true;
        return $this;
    }

    // caching helpers

    private function isCacheable(ZoomApiClientInterface $client, ZoomApiEndpoint $endpoint, $method, $arguments)
    {
        // reset temp cache bypass
        $bypass = $this->cacheBypassed;
        $this->cacheBypassed = false;

        if ($bypass) {
            return false;
        }

        if (!config('zoom-api.cache_active')) {
            return false;
        }

        if (strtolower($endpoint->getMethod()) !== 'get') {
            return false;
        }

        $cachedCallsForClient = config('zoom-api.cacheable_calls.' . get_class($client));
        if (is_null($cachedCallsForClient)) {
            return false;
        }

        if ($cachedCallsForClient !== ['*'] && !in_array($method, $cachedCallsForClient)) {
            return false;
        }

        return true;
    }

    public function refreshToken()
    {
        $tokenDriver = config('zoom-api.token_storage.driver');
        if ($tokenDriver !== 'builtin-db') {
            return;
        }

        $token = ZoomApiToken::active($this->config);
        $now = Carbon::now();

        if (is_null($token) || $now->gte($token->expires_at)) {
            $ttlSeconds = config('zoom-api.token_storage.ttl');

            // refresh the token on the Config object
            $jwt = $this->config->refreshToken($ttlSeconds);

            // store the token in DB
            $token = ZoomApiToken::create([
                'token'        => $jwt,
                'config_class' => get_class($this->config),
                'retrieved_at' => Carbon::now()->toDateTimeString(),
                'expires_at'   => Carbon::createFromTimestamp(time() + $ttlSeconds)->toDateTimeString(),
            ]);
        } else {
            $this->config->setJwt($token->token);
        }

        return $token;
    }
}

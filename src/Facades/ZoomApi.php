<?php

namespace Uncgits\ZoomApiLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class ZoomApi extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ZoomApi'; // the IoC binding.
    }
}

<?php

namespace Uncgits\ZoomApiLaravel\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ApiRequestFinished
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $client;
    protected $endpoint;
    protected $method;
    protected $arguments;
    protected $result;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($client, $endpoint, $method, $arguments, $result)
    {
        $this->client = $client;
        $this->endpoint = $endpoint;
        $this->method = $method;
        $this->arguments = $arguments;
        $this->result = $result;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('zoom-api');
    }
}

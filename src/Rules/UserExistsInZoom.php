<?php

namespace Uncgits\ZoomApiLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;

class UserExistsInZoom implements Rule
{
    private $failedUser;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $getUserResult = \ZoomApi::using('users')->getUser($value);

        if ($getUserResult->getStatus() === 'success') {
            return true;
        }

        $this->failedUser = $value;
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User ' . $this->failedUser . ' does not exist in this environment';
    }
}

<?php

namespace Uncgits\ZoomApiLaravel;

use Uncgits\ZoomApi\ZoomApiConfig;
use Illuminate\Database\Eloquent\Model;

class ZoomApiToken extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'retrieved_at',
        'expires_at'
    ];

    // overrides
    const CREATED_AT = 'retrieved_at';

    // accessors
    public function getObfuscatedTokenAttribute()
    {
        return substr($this->token, 0, 10) . '**********';
    }

    // token encryption
    public function setTokenAttribute($value)
    {
        $this->attributes['token'] = encrypt($value);
    }
    public function getTokenAttribute($value)
    {
        return decrypt($value);
    }

    // other methods
    public static function active(ZoomApiConfig $config)
    {
        return self::where('config_class', get_class($config))
            ->latest()
            ->first();
    }
}

<?php

namespace Uncgits\ZoomApiLaravel;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register artisan commands, other CLI stuff
        if ($this->app->runningInConsole()) {
            // publish config
            $this->publishes([
                __DIR__ . '/publish/config' => base_path('config'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/publish/database/migrations' => base_path('database/migrations')
            ], 'migrations');
        }
    }



    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ZoomApi', function ($app) {
            return new ZoomApi();
        });
    }
}

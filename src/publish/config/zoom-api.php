<?php

return [

    // configurations

    'defaults' => [
        'config'  => 'default-account', // config key where 'class' is set
        'adapter' => \Uncgits\ZoomApi\Adapters\Guzzle::class, // adapter class
    ],

    'configs' => [
        'default-account' => [
            'class'          => \App\ZoomApiConfigs\DefaultAccount::class,
            'key'            => env('ZOOM_API_DEFAULT_KEY'),
            'secret'         => env('ZOOM_API_DEFAULT_SECRET'),
            'proxy'          => [
                'use'  => env('ZOOM_API_DEFAULT_USE_HTTP_PROXY', 'false') == 'true',
                'host' => env('ZOOM_API_DEFAULT_HTTP_PROXY_HOST'),
                'port' => env('ZOOM_API_DEFAULT_HTTP_PROXY_PORT'),
            ],
        ],
        'other-account' => [
            'class'          => \App\ZoomApiConfigs\OtherAccount::class,
            'public_key'     => env('ZOOM_API_OTHER_KEY'),
            'secret'         => env('ZOOM_API_OTHER_SECRET'),
            'proxy'          => [
                'use'  => env('ZOOM_API_OTHER_USE_PROXY', 'false') == 'true',
                'host' => env('ZOOM_API_OTHER_PROXY_HOST'),
                'port' => env('ZOOM_API_OTHER_PROXY_PORT'),
            ],
        ],
        'third-account' => [
            'class'          => \App\ZoomApiConfigs\ThirdAccount::class,
            'public_key'     => env('ZOOM_API_THIRD_KEY'),
            'secret'         => env('ZOOM_API_THIRD_SECRET'),
            'proxy'          => [
                'use'  => env('ZOOM_API_THIRD_USE_PROXY', 'false') == 'true',
                'host' => env('ZOOM_API_THIRD_PROXY_HOST'),
                'port' => env('ZOOM_API_THIRD_PROXY_PORT'),
            ],
        ],
    ],

    // Token handling
    'token_storage' => [
        'driver' => env('ZOOM_API_TOKEN_DRIVER', 'builtin-db'),
        'ttl'    => env('ZOOM_API_TOKEN_TTL', 3600) // 1 hour
    ],

    // use collections?
    'use_collections' => env('ZOOM_API_USE_COLLECTIONS', true),

    // caching

    'cache_active'    => env('ZOOM_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes'   => env('ZOOM_API_CACHE_MINUTES', 10),

    // cache these specific GET requests by client class. use * to cache all
    'cacheable_calls' => [
        // 'Uncgits\ZoomApi\Clients\Users' => [
        //     'listUsers',
        //     'getUser'
        // ],
        // 'Uncgits\ZoomApi\Clients\Groups' => ['*']
    ],

];

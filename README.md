# Zoom API Library - Laravel Wrapper

This package is a Laravel wrapper for the UNCG Zoom API PHP Library package, so that the Zoom API PHP Library can be used in Laravel apps.

---

# Installation

1. `composer require uncgits/zoom-api-wrapper-laravel`
2. *IF running Laravel 5.4 or below*: Add `Uncgits\ZoomApiLaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\ZoomApiLaravel\ServiceProvider' --tag=config` - to publish the `zoom-api.php` config file
4. IF USING THE BUILT-IN DATABASE TOKEN DRIVER, run `php artisan vendor:publish --provider='Uncgits\ZoomApiLaravel\ServiceProvider' --tag=migrations` - to publish the database migration file, and then run `php artisan migrate` to run it.
5. Set your environment credentials in your `.env` file, and set your configuration options in `config/zoom-api.php`

## Dependencies

This package has a dependency on `uncgits/zoom-api-php-library`

# Quick Start

## Config and `.env` setup

You'll want to fill in details on your accounts(s) in the published `zoom-api.php` file. Most often you likely have only one set of credentials to use, but if you are managing multiple Zoom environments from the same application, the flexibility is here for you to put in multiple sets of keys/secrets. Take a look at the default placeholders in the config file and modify them to whatever convention suits you. You will also want to modify the references to the `.env` variables to something that makes sense.

In general, you should consider making **one config file per set of public/private keys you will be using in Zoom**.

## Set up Config class(es)

In accordance with the documentation on the `uncgits/zoom-api-php-library` package, set up a Config class for each set of Zoom credentials you wish to use with this package. A recommended namespacing would be `App\ZoomApiConfigs`.

## `.env` variables

At minimum, you should add the account and public/private information for the environment(s) you plan to use with this library.

> Note: do not use protocols (http:// or https://) in your host information

```
ZOOM_API_DEFAULT_ACCOUNT_KEY=
ZOOM_API_DEFAULT_ACCOUNT_SECRET=
```

> These keys are just a suggestion - use whatever you want as you customize your app's `zoom-api.php` config file and `.env` file

## Set default Client and Adapter

In the `zoom-api.php` config file, fill in both of the items under `defaults`. `defaults.config` should be another config key for that same file that points to the entry in `configs` that you want to use. `defaults.adapter` should point to whichever Adapter class you want to use (either `::class` syntax or the full name as a string will be fine there).

Once you finish this step you should be ready to make your Zoom API calls!

## Setup token storage

Out of the box, this package supports tokens storage per-config via the database. JWTs are stored encrypted at-rest. Publish the default database migration for this (`php artisan vendor:publish --provider='Uncgits\ZoomApiLaravel\ServiceProvider' --tag=migrations`), run your migrations (`php artisan migrate`), and then you should be able to use the feature.

If your needs extend beyond the basic for this, then you will need to create a wrapper class for `ZoomApi` that implements a different `refreshToken()` method. Currently the only supported built-in option for this is the `builtin-db` configuration; this may change in future.

---

# Usage

Before proceeding here, please ensure you've read and are familiar with the details of the `uncgits/zoom-api-php-library` package.

## Use the facade

The recommended usage for this wrapper package involves the `ZoomApi` root-level facade. Note that this requires you to set the Client you're using (e.g. Accounts, Users, etc.) on each call, however:

```php
$result = \ZoomApi::using('users')->listUsers();
```

## Instantiate the `ZoomApi` class itself

If you prefer, you can instantiate the `Uncgits\ZoomApiLaravel\ZoomApi` class and set a Client class on that instance instead, for better permanence:

```php
$api = new Uncgits\ZoomApiLaravel\ZoomApi; // Config and Adapter classes are pulled in based on defaults in the config file
$api->setClient(new Uncgits\ZoomApi\Clients\Users); // this is not discernable via a default setting, so it must be set.

$result = $api->listUsers();
```

## Configuration options

### Logging

If you wish to implement logging or flash messages or anything of the sort, it is recommended to create a listener that hooks into the `ApiRequestFinished` event, and perform your logic for logging / messages / etc. from there.

### Caching

For speed, adherence to rate limits, and overall performance, caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `ZOOM_API_CACHING=off` to your `.env` file.

#### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `ZOOM_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes instead.

#### Cached clients / methods

Caching is performed only for specified clients/methods, and is only active for `GET` requests (obviously, you would not want to cache requests that are designed to actually modify information in Zoom, like `POST` or `PUT` or `DELETE` requests).

To specify which Clients should be cached, utilize the `cacheable_calls` array in the `zoom-api.php` config file:

```php
'cacheable_calls' => [
    // use the Client as the key for an array entry
    'Uncgits\ZoomApi\Clients\Users' => [
        // list each method name you wish to cache here. aliases need to be listed separately!
        'listUsers',
        'getUser',
        // note that any alias methods you use need to be listed here as well - check the Client class to be sure.
    ],

    // to cache all GET transactions made with a client, just use an asterisk instead
    'Uncgits\ZoomApi\Clients\Groups' => ['*']
],
```

#### Determining whether the cache was used

The `ZoomApi` class in this package will return a `ZoomApiResult` class that is an extension of the original, adding two properties: `$source` and `$cacheKey`. You can access either of these on the object using standard getters `getSource()` and `getCacheKey()`.

#### Bypassing the cache

On an individual API call, you can chain `withoutCache()` to bypass the cache on that individual transaction, regardless of what the overall settings are in the application.

### Collections

By default this library casts the result of list-style calls (calls that have multiple results) into collections instead of arrays, as a convenience for Laravel. If you wish to disable this, you can change the `use_collections` key in the config file, or use the `ZOOM_API_USE_COLLECTIONS` key in your `.env` file.

## Events

When an API transaction is initiated, the `ApiRequestStarted` event will be dispatched. Similarly, as the result is returned, the `ApiRequestFinished` event will be dispatched. Nothing is set by default to listen to these events, but you can hook into them as needed in your application.

As recommended above, listeners would be a great place to implement logging or other recording of API requests / results in your application.

## Validation Rules

One validation rule is provided by default as an example of some of the things you could add to your forms. Feel free to extend this and/or write your own; pull requests for additional rules would be welcome!

---

# Contributing

Please feel free to submit a pull request to this package to help improve it or fix issues.

# License

See the [LICENSE](LICENSE.md) file for license rights and limitations (BSD).

# Questions? Concerns?

Please use the Issue Tracker on this repository for reporting bugs. For security-related concerns, please contact its-laravel-devs-l@uncg.edu directly instead of using the Issue Tracker.

---

# Version History

## 1.1

- Refactors token acquisition to account for JWT being handled at the PHP Library level
- Addresses a potential token mixup situation when using multiple configs
- Changes default token store to `builtin-db` so that it works out of the box

## 1.0.1

- Adds BSD licensing to `composer.json`

## 1.0

- Official open source licensing

## 0.4.2

- tweak to remove the `first()` check when getting content and using collections

## 0.4.1

- fixes for collections

## 0.4

- adds option to use collections in the `getContent()` method.
- true port for `UserExistsInZoom` rule
- require `^0.4` from `uncgits/zoom-api-php-library`

## 0.3

- require `^0.3` from `uncgits/zoom-api-php-library`

## 0.2.4

- better publishing for the token database migration

## 0.2.3

- require `^0.2` from `uncgits/zoom-api-php-library`

## 0.2.2

- PSR-4 declaration fix

## 0.2.1

- Fix for token-setting when caching is active

## 0.2

- Token refreshing and built-in database option

## 0.1

- Initial release.
